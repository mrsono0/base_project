FROM vitorfec/oracle-xe-18c as vscode-server
# https://hub.docker.com/r/vitorfec/oracle-xe-18c
# https://github.com/oracle/docker-images/blob/master/OracleDatabase/SingleInstance/dockerfiles/18.4.0/Dockerfile.xe
ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 SHELL=/bin/bash
ENV LANGUAGE=${LANG} TZ=Asia/Seoul
ENV PATH=/usr/local/coder:/usr/local/bin:$PATH

RUN yum update -y \
    && yum install -y sudo \
    nodejs \
    wget \
    curl \
    oracle-epel-release-el7 oracle-release-el7 python3 \
    python3-devel python3-setuptools python3-pip nss_wrapper \
    httpd httpd-devel atlas-devel gcc-gfortran libffi-devel \
    libtool-ltdl enchant redhat-rpm-config dumb-init
# https://github.com/cdr/code-server/releases
ENV vscode_version=3.4.1
ENV vscode_filename=code-server-3.4.1-linux-x86_64
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support for non-root user
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && mkdir -p /usr/local/coder \
    && wget https://github.com/cdr/code-server/releases/download/${vscode_version}/${vscode_filename}.tar.gz \
    && tar -xvf ${vscode_filename}.tar.gz \
    && mv ${vscode_filename} coder \
    && mv coder /usr/local/ \
    && chmod +x /usr/local/coder/code-server \
    && ln -s /usr/local/coder/code-server /usr/local/bin/code-server \
    && rm -f ${vscode_filename}.tar.gz 

RUN unset vscode_version vscode_filename
# COPY ETC/entrypoint.sh /usr/local/bin
# RUN chmod +x /usr/local/bin/entrypoint.sh

# RUN locale-gen --purge
# RUN locale-gen ko_KR.UTF-8
FROM vscode-server as python3

# ENV PYTHON_VERSION=3.7 \
#     PATH=$HOME/.local/bin/:$PATH \
#     PYTHONUNBUFFERED=1 \
#     PYTHONIOENCODING=UTF-8 \
#     LC_ALL=en_US.UTF-8 \
#     LANG=en_US.UTF-8 \
#     PIP_NO_CACHE_DIR=off

# ENV NAME=python3 \
#     VERSION=0 \
#     ARCH=x86_64

# RUN INSTALL_PKGS="python3 python3-devel python3-setuptools python3-pip nss_wrapper \
#         httpd httpd-devel atlas-devel gcc-gfortran libffi-devel \
#         libtool-ltdl enchant redhat-rpm-config" && \
#     yum -y --setopt=tsflags=nodocs install $INSTALL_PKGS && \
#     rpm -V $INSTALL_PKGS && \
#     yum -y clean all --enablerepo='*'

RUN bash -c "pip3 install --no-input jupyter" \
	&& bash -c "pip3 install --no-input jupyterlab" \
	&& bash -c "pip3 install --no-input pylint" 

COPY ETC/entrypoint.oraxe18c.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

EXPOSE 6006-6015 8080 8888 8889
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]