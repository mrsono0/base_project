FROM mrsono0/base_project:selenium

# https://github.com/ContinuumIO/docker-images/blob/master/anaconda3/debian/Dockerfile
# https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh
RUN wget --quiet https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh -O ~/anaconda.sh && \
  /bin/bash ~/anaconda.sh -b -p /opt/conda && \
  rm ~/anaconda.sh && \
  /opt/conda/bin/conda clean -tipsy && \
  ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
  echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
  echo "conda activate base" >> ~/.bashrc && \
  find /opt/conda/ -follow -type f -name '*.a' -delete && \
  find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
  /opt/conda/bin/conda clean -afy


ENV PATH=/opt/conda/bin:$PATH
RUN if [ ! -d "/usr/local/anaconda3" ]; then ln -s /opt/conda /usr/local/anaconda3; fi
RUN pip install --upgrade pip
RUN bash -c "/opt/conda/bin/conda install jupyter -y --quiet" && \
  bash -c "/opt/conda/bin/conda install jupyterlab -y --quiet" && \
  bash -c "/opt/conda/bin/conda install pylint -y --quiet"

# Generating a default config during build time
RUN /usr/local/bin/generate_config > /opt/selenium/config.json \ 
  && conda install -c conda-forge pyvirtualdisplay selenium -y \
  && pip install fake_useragent

COPY ETC/entrypoint.crawling.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

# CMD ["jupyter", "lab", "--notebook-dir=/home/vscode/notebooks", "--ip='*'", "--port=8888", "--no-browser", "--allow-root", "--NotebookApp.token=''", "--NotebookApp.password=''"]