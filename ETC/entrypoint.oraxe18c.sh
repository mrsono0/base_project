#!/bin/bash

code-server --host 0.0.0.0 --auth none --port 8889 &

if [[ "${JUPYTER_RUN}" = "yes" ]]; then
   jupyter lab --ip='*' --port=8888 --no-browser --allow-root --NotebookApp.token='' --NotebookApp.password='' &
fi

/opt/oracle/runOracle.sh && tail -f /dev/null &
exec "$@"
