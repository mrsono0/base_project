#!/bin/bash

dumb-init code-server --host 0.0.0.0 --auth none --port 8889 &

if [[ "${JUPYTER_RUN}" = "yes" ]]; then
   dumb-init jupyter lab --ip='*' --port=8888 --no-browser --allow-root --NotebookApp.token='' --NotebookApp.password='' &
fi

dumb-init /usr/sbin/startup.sh && tail -f /dev/null &
# /u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh
exec "$@"
