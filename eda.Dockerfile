FROM mrsono0/base_project:anaconda3 as crawling

RUN set -eux; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
  gzip unzip zip \
  ; \
  apt-get clean; \
  rm -rf /var/lib/apt/lists/*

ARG JAVA_VERSION=8u251
COPY ETC/jdk-${JAVA_VERSION}-linux-x64.tar.gz /usr/local/
RUN cd /usr/local; tar -zxvf jdk-${JAVA_VERSION}-linux-x64.tar.gz; rm /usr/local/jdk-${JAVA_VERSION}-linux-x64.tar.gz ; cd /

ENV JAVA_HOME /usr/local/jdk1.8.0_251
ENV PATH $JAVA_HOME/bin:$PATH
RUN echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
  && echo "export PATH=${JAVA_HOME}/bin:$PATH" >> /etc/profile \
  && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
  && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi 

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
  apt-get install -y \
  procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
  && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
  #
  # Install git, process tools, lsb-release (common in install instructions for CLIs)
  && apt-get -y install procps lsb-release \
  #
  # Clean up
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

ARG TOMCAT_VERSION=9.0.37
RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz \
  && tar xzf apache-tomcat-${TOMCAT_VERSION}.tar.gz; rm -rf apache-tomcat-${TOMCAT_VERSION}.tar.gz \
  && mv apache-tomcat-${TOMCAT_VERSION} /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ARG KOTLIN_VERSION=1.3.70
RUN cd /usr/lib && \
  wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
  unzip kotlin-compiler-*.zip && \
  rm kotlin-compiler-*.zip && \
  rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION TOMCAT_VERSION KOTLIN_VERSION MAVEN_VERSION

ARG URL=https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y wget gdebi-core fonts-naver-d2coding
RUN wget -q -O /tmp/google-chrome-stable_current_amd64.deb ${URL}
RUN DEBIAN_FRONTEND=noninteractive gdebi --n /tmp/google-chrome-stable_current_amd64.deb
RUN apt-get install -y libcanberra-gtk3-module

COPY ETC/wrap_chrome_binary /usr/local/bin/wrap_chrome_binary
RUN chmod +x /usr/local/bin/wrap_chrome_binary \
  && /usr/local/bin/wrap_chrome_binary

ARG CHROME_DRIVER_VERSION
RUN if [ -z "$CHROME_DRIVER_VERSION" ]; \
  then CHROME_MAJOR_VERSION=$(google-chrome --version | sed -E "s/.* ([0-9]+)(\.[0-9]+){3}.*/\1/") \
  && CHROME_DRIVER_VERSION=$(wget --no-verbose -O - "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROME_MAJOR_VERSION}"); \
  fi \
  && echo "Using chromedriver version: "$CHROME_DRIVER_VERSION \
  && wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
  && rm -rf /opt/selenium/chromedriver \
  && unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
  && rm /tmp/chromedriver_linux64.zip \
  && mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && chmod 755 /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && sudo ln -fs /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver

COPY ETC/generate_config /usr/local/bin/generate_config
RUN chmod +x /usr/local/bin/generate_config

RUN /usr/local/bin/generate_config > /opt/selenium/config.json \ 
  && conda install -c conda-forge pyvirtualdisplay selenium -y \
  && pip install fake_useragent

FROM crawling as oracle-instantclient19.6
RUN apt-get -y install --no-install-recommends alien libaio1
ADD ./ETC/assets/oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm /oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm 
RUN cd / \
  && alien -iv oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm \
  && rm -rf oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm
# RUN dpkg -i oracle-instantclient.deb

ENV ORACLE_HOME /usr/lib/oracle/19.6/client64
ENV TNS_ADMIN /usr/lib/oracle/19.6/client64/lib/network/admin
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${ORACLE_HOME}/lib

# Clean up
RUN apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

# RUN bash -c "/opt/conda/bin/conda install -c anaconda cx_Oracle mysql psycopg2 sqlite sqlalchemy -y --quiet"
# RUN conda install -c conda-forge ipython-sql -y --quiet
# RUN python -m pip install cx_Oracle --upgrade

RUN python -m pip install --upgrade pip
RUN pip install ipython-sql cx_Oracle mysql psycopg2 pysqlite3 sqlalchemy
RUN python -m pip install cx_Oracle --upgrade

RUN pip install beakerx pandas py4j \
  && beakerx install 
# && cd /usr/local/share/jupyter/kernels; rm -r clojure groovy scala sql

EXPOSE 4444 6006-6015 8080 8888 8889

COPY ETC/entrypoint.eda.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]