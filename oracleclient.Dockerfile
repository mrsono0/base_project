FROM mrsono0/base_project:anaconda3

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	curl zip gzip unzip wget \
	;

ENV JAVA_FILE jdk-11.0.5_linux-x64_bin.tar.gz
COPY ETC/${JAVA_FILE} /usr/local/
RUN cd /usr/local; tar -zxvf ${JAVA_FILE}; rm /usr/local/${JAVA_FILE}; ls -al; cd /

ENV JAVA_HOME /usr/local/jdk-11.0.5
ENV PATH $JAVA_HOME/bin:$PATH
# RUN update-alternatives --config java

	# basic smoke test
RUN	javac -version; \
	java -version

## https://github.com/SpencerPark/IJava
RUN wget https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip
RUN mv ijava-1.3.0.zip /usr/local/; cd /usr/local; unzip ijava-1.3.0.zip; python /usr/local/install.py --sys-prefix; rm -r java install.py ijava-1.3.0.zip


ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
      curl procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi 

ARG TOMCAT_VERSION=9.0.36
RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz \
    && tar xzf apache-tomcat-${TOMCAT_VERSION}.tar.gz; rm -rf apache-tomcat-${TOMCAT_VERSION}.tar.gz \
    && mv apache-tomcat-${TOMCAT_VERSION} /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.70
RUN cd /usr/lib && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

# RUN bash -c "/opt/conda/bin/conda install -c anaconda cx_Oracle mysql psycopg2 sqlite sqlalchemy -y --quiet"
# RUN conda install -c conda-forge ipython-sql -y --quiet
# RUN python -m pip install cx_Oracle --upgrade

RUN python -m pip install --upgrade pip
RUN pip install ipython-sql cx_Oracle mysql psycopg2 pysqlite3 sqlalchemy
RUN python -m pip install cx_Oracle --upgrade

RUN apt-get -y install --no-install-recommends alien libaio1
ADD ./ETC/assets/oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm /oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm 
RUN cd / \
    && alien -iv oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm \
    && rm -rf oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm
# RUN dpkg -i oracle-instantclient.deb

ENV ORACLE_HOME /usr/lib/oracle/19.6/client64
ENV TNS_ADMIN /usr/lib/oracle/19.6/client64/lib/network/admin
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${ORACLE_HOME}/lib

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*