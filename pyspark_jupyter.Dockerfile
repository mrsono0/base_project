FROM jupyter/all-spark-notebook

# RUN pip install beakerx pandas py4j \
#     && beakerx install \
#     && cd /opt/conda/share/jupyter/kernels; rm -r clojure groovy scala

USER root

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=vscode
ARG USER_UID=1001
ARG USER_GID=$USER_UID

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 SHELL=/bin/bash
ENV LANGUAGE=${LANG} TZ=Asia/Seoul
ENV PATH=/usr/local/coder:/usr/local/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget curl bzip2 libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion \
    bash-completion \
    ca-certificates \
    software-properties-common \
    apt-transport-https \
    tzdata vim nano neovim git-lfs \
    openssl locales dumb-init \
    supervisor \
    cron \
    gpg \
    dirmngr \
    iputils-ping

# timezone settings
RUN cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo $TZ > /etc/timezone

# https://github.com/cdr/code-server/releases
ENV vscode_version=3.4.1
ENV vscode_filename=code-server-3.4.1-linux-x86_64
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support for non-root user
    && apt-get install -y sudo \
    nodejs \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && mkdir -p /usr/local/coder \
    && wget https://github.com/cdr/code-server/releases/download/${vscode_version}/${vscode_filename}.tar.gz \
    && tar -xvf ${vscode_filename}.tar.gz \
    && mv ${vscode_filename} coder \
    && mv coder /usr/local/ \
    && chmod +x /usr/local/coder/code-server \
    && ln -s /usr/local/coder/code-server /usr/local/bin/code-server \
    && rm -f ${vscode_filename}.tar.gz \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN locale-gen --purge
RUN locale-gen ko_KR.UTF-8

RUN unset vscode_version vscode_filename
COPY ETC/entrypoint.pyspark_jupyter.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

RUN conda install -c conda-forge jupyter_contrib_nbextensions python-hdfs py4j

EXPOSE 8080 8888 8889 7077 6066 4040
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]