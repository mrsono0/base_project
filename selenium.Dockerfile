FROM selenium/standalone-chrome

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

USER root

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 SHELL=/bin/bash
ENV LANGUAGE=${LANG} TZ=Asia/Seoul
ENV PATH=/usr/local/coder:/usr/local/bin:$PATH

RUN apt-get update --fix-missing && \
  apt-get install -y wget curl bzip2 libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion \
  bash-completion \
  ca-certificates \
  software-properties-common \
  apt-transport-https \
  tzdata vim nano neovim git-lfs \
  openssl locales dumb-init \
  supervisor \
  cron \
  gpg \
  dirmngr 

# timezone settings
RUN cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo $TZ > /etc/timezone

# https://github.com/cdr/code-server/releases
ENV vscode_version=3.4.1
ENV vscode_filename=code-server-3.4.1-linux-x86_64
RUN groupadd --gid $USER_GID $USERNAME \
  && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
  # [Optional] Add sudo support for non-root user
  && apt-get install -y sudo \
  nodejs \
  && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
  && chmod 0440 /etc/sudoers.d/$USERNAME \
  && mkdir -p /usr/local/coder \
  && wget https://github.com/cdr/code-server/releases/download/${vscode_version}/${vscode_filename}.tar.gz \
  && tar -xvf ${vscode_filename}.tar.gz \
  && mv ${vscode_filename} coder \
  && mv coder /usr/local/ \
  && chmod +x /usr/local/coder/code-server \
  && ln -s /usr/local/coder/code-server /usr/local/bin/code-server \
  && rm -f ${vscode_filename}.tar.gz \
  # Clean up
  && apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

RUN locale-gen --purge \
  && locale-gen ko_KR.UTF-8

# Install dependencies.
RUN apt-get update
RUN apt-get install -y xvfb unzip libxi6 libgconf-2-4 gnupg gnupg2 gnupg1 \
  p11-kit \
  sudo \
  unzip \
  wget \
  jq \
  curl \
  supervisor \
  gnupg2

#==============
# Xvfb
#==============
RUN apt-get update -qqy \
  && apt-get -qqy install \
  xvfb 

#================
# Font libraries
#================
RUN apt-get -qqy update \
  && apt-get -qqy --no-install-recommends install \
  libfontconfig \
  libfreetype6 \
  xfonts-cyrillic \
  xfonts-scalable \
  fonts-liberation \
  fonts-ipafont-gothic \
  fonts-wqy-zenhei \
  fonts-tlwg-loma-otf \
  fonts-naver-d2coding \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get -qyy clean

#==============================
# Scripts to run Selenium Node and XVFB
#==============================
COPY ETC/start-selenium-node.sh \
  ETC/start-xvfb.sh \
  /usr/local/bin/
RUN chmod +x /usr/local/bin/start-selenium-node.sh
RUN chmod +x /usr/local/bin/start-xvfb.sh

#============================
# Some configuration options
#============================
ENV SCREEN_WIDTH 1360
ENV SCREEN_HEIGHT 1020
ENV SCREEN_DEPTH 24
ENV SCREEN_DPI 96
ENV DISPLAY :99.0
ENV START_XVFB true

#========================
# Selenium Configuration
#========================
# As integer, maps to "maxInstances"
ENV NODE_MAX_INSTANCES 1
# As integer, maps to "maxSession"
ENV NODE_MAX_SESSION 1
# As address, maps to "host"
ENV NODE_HOST 0.0.0.0
# As integer, maps to "port"
ENV NODE_PORT 5555
# In milliseconds, maps to "registerCycle"
ENV NODE_REGISTER_CYCLE 5000
# In milliseconds, maps to "nodePolling"
ENV NODE_POLLING 5000
# In milliseconds, maps to "unregisterIfStillDownAfter"
ENV NODE_UNREGISTER_IF_STILL_DOWN_AFTER 60000
# As integer, maps to "downPollingLimit"
ENV NODE_DOWN_POLLING_LIMIT 2
# As string, maps to "applicationName"
ENV NODE_APPLICATION_NAME ""
# Debug
ENV GRID_DEBUG false

# Following line fixes https://github.com/SeleniumHQ/docker-selenium/issues/87
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

# Creating base directory for Xvfb
RUN mkdir -p /tmp/.X11-unix && sudo chmod 1777 /tmp/.X11-unix

#============================================
# Google Chrome
#============================================
# can specify versions by CHROME_VERSION;
#  e.g. google-chrome-stable=53.0.2785.101-1
#       google-chrome-beta=53.0.2785.92-1
#       google-chrome-unstable=54.0.2840.14-1
#       latest (equivalent to google-chrome-stable)
#       google-chrome-beta  (pull latest beta)
#============================================
# ARG CHROME_VERSION="google-chrome-stable"
# RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
#   && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
#   && apt-get update -qqy \
#   && apt-get -qqy install \
#   ${CHROME_VERSION:-google-chrome-stable} \
#   fonts-naver-d2coding \
#   && rm /etc/apt/sources.list.d/google-chrome.list \
#   && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ARG URL=https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y wget gdebi-core fonts-naver-d2coding
RUN wget -q -O /tmp/google-chrome-stable_current_amd64.deb ${URL}
RUN DEBIAN_FRONTEND=noninteractive gdebi --n /tmp/google-chrome-stable_current_amd64.deb
RUN apt-get install -y libcanberra-gtk3-module fonts-ubuntu light-themes

#=================================
# Chrome Launch Script Wrapper
#=================================
COPY ETC/wrap_chrome_binary /usr/local/bin/wrap_chrome_binary
RUN chmod +x /usr/local/bin/wrap_chrome_binary \
  && /usr/local/bin/wrap_chrome_binary

#============================================
# Chrome webdriver
#============================================
# can specify versions by CHROME_DRIVER_VERSION
# Latest released version will be used by default
#============================================
# ENV CHROME_MAJOR_VERSION 84.0.4147.30
# ENV CHROME_DRIVER_VERSION LATEST_RELEASE_${CHROME_MAJOR_VERSION}
# RUN wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_MAJOR_VERSION/chromedriver_linux64.zip \
#   && rm -rf /opt/selenium/chromedriver \
#   && unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
#   && rm /tmp/chromedriver_linux64.zip \
#   && mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
#   && chmod 755 /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
#   && sudo ln -fs /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver
ARG CHROME_DRIVER_VERSION
RUN if [ -z "$CHROME_DRIVER_VERSION" ]; \
  then CHROME_MAJOR_VERSION=$(google-chrome --version | sed -E "s/.* ([0-9]+)(\.[0-9]+){3}.*/\1/") \
  && CHROME_DRIVER_VERSION=$(wget --no-verbose -O - "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROME_MAJOR_VERSION}"); \
  fi \
  && echo "Using chromedriver version: "$CHROME_DRIVER_VERSION \
  && wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
  && rm -rf /opt/selenium/chromedriver \
  && unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
  && rm /tmp/chromedriver_linux64.zip \
  && mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && chmod 755 /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && sudo ln -fs /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver

COPY ETC/generate_config /usr/local/bin/generate_config
RUN chmod +x /usr/local/bin/generate_config

# Clean up
RUN apt-get autoremove -y \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*

RUN unset vscode_version vscode_filename
COPY ETC/entrypoint.selenium.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

EXPOSE 4444 8080 8888 8889
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]