FROM mrsono0/base_project:oraclejdk11

ENV DEBIAN_FRONTEND noninteractive
RUN sed -i "s/archive.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/security.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/# deb-src/deb-src/g" /etc/apt/sources.list
RUN apt-get -y update --fix-missing \
    && apt-get -yy upgrade \
    && apt-get -yy install p7zip-full
USER root
COPY ETC/assets /assets
RUN chown -R root:root /assets \
    && chmod -R +x /assets/* \
    && /assets/setup.sh

ENV ORACLE_LIBDIR /u01/app/oracle/product/11.2.0/xe/lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${ORACLE_LIBDIR}
ENV ORA_TZFILE /u01/app/oracle/product/11.2.0/xe/oracore/zoneinfo/timezlrg_14.dat

RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install ipython-sql cx_Oracle mysql psycopg2 pysqlite3 sqlalchemy
RUN python -m pip install cx_Oracle --upgrade

RUN apt-get -y install --no-install-recommends alien libaio1
ADD ./ETC/assets/oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm /oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm 
RUN cd / \
    && alien -iv oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm \
    && rm -rf oracle-instantclient19.6-basic-19.6.0.0.0-1.x86_64.rpm

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY ETC/entrypoint.oraxe11g.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
EXPOSE 1521 8080 8888 8889
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD [ "/bin/bash" ]
# CMD /usr/sbin/startup.sh && tail -f /dev/null