FROM mrsono0/base_project:nodejs12

RUN apt-get update \
    && apt-get install -y python build-essential
RUN yarn add global @tensorflow/tfjs @tensorflow/tfjs-node